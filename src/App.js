import React, {Fragment} from 'react';
import {BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Navbar from './Components/navbarDefault';
import Anuncio from './pages/Anuncio';
import Login from './pages/auth/Login';
import Registro from './pages/auth/Register';
import Home from './pages/Home';
import Settings from './pages/Settings';

function App() {
  return (
    <div className="">
      <Fragment>
        <Router>
          <Routes>
            <Route path='/' exact element={<Home/>} />
            <Route path='/login' exact element={<Login/>} />
            <Route path='/register' exact element={<Registro />} />
            <Route path="/anuncio/buscar/:id"  element={<Anuncio />} />
            <Route path='/settings' exact element={<Settings />} />
            <Route path='/testing/:id' exact element={<Anuncio/>} />


          </Routes>
        </Router>
      </Fragment>

    </div>
  );
}

export default App;
