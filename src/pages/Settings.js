import React, {useState , useEffect} from "react";
import { Link } from "react-router-dom";
import APIInvoke from "../APIInvoke";
import swal from 'sweetalert';


const Settings = () => {

useEffect( () => {
    
        async function fetchData() {
            const response = await APIInvoke.invokePOST(`/`, {} );

            if( response.error !== undefined ){
               // navigate('/login');
                return;
            }

            document.getElementById("nombre").innerHTML = response.data.nombre;
            document.getElementById("correo").innerHTML = response.data.correo;
        }

        fetchData();
            }, []);
    
    return(
     <div>
        <span id='nombre'/>
<div>        <span id='correo'/></div>
     </div>   
    )
};

export default Settings;