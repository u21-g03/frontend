import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import APIInvoke from "../APIInvoke";
import Footer from "../Components/Footer";
import Navbar from "../Components/navbarDefault";

const Anuncio = () => {
    
    const navigate = useNavigate();
    
    const {id} = useParams();

    const [comentario, setComentario] = useState(
        
        []
        
        /*{
            _id:"",
            idUsuario:"",
            contenido:"",
            fecha:""
        }*/
    )

    const [anuncio, setAnuncio] = useState(
        {
            titulo:"",
            descripcion:"",
            entrega:"",
            _id:"",
            categoria:"",
            ubicacion:[],
            precio:0,
            cantidad:0,
            foto:[],
            creado:"",

        }
    );

    const {titulo, descripcion, entrega, _id, categoria, ubicacion, precio, cantidad, foto, creado} = anuncio;
    
    useEffect(() => {
        
        async function cargarAnuncio (){
            const response = await APIInvoke.invokeGET(`/anuncios/${id}`);
            setAnuncio(response);

            return;
        }

        async function cargarComentarios() {
            const respons = await APIInvoke.invokeGET(`/anuncio/${id}/comentario`);
            setComentario(respons);
console.log(respons);
            return;
            
        }

        cargarAnuncio();
        cargarComentarios();
        document.getElementById("titulo").focus();
    },[]);

    return (

    <div className="hold-transition sidebar-mini">
        {/* Site wrapper */}
        <div className="wrapper">
            {/* Content Wrapper. Contains page content */}
            <Navbar></Navbar>

                {/* Content Header (Page header) */}
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6"
                            id="id"
                            name="id"
                            value={ _id }
                            >
                                <h1>{titulo}</h1>
                            </div>

                        </div>
                    </div>{/* /.container-fluid */}
                </section>

                {/* Main content */}
                <section className="content">
                    {/* Default box */}
                    <div className="card card-solid">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-12 col-sm-6">
                                    <h3 className="d-inline-block d-sm-none">LOWA Men’s Renegade GTX Mid Hiking Boots Review</h3>
                                    <div className="col-12">
                                        <img src="../../dist/img/prod-1.jpg" className="product-image" alt="Product Image" />
                                    </div>
                                    <div className="col-12 product-image-thumbs">
                                        <div className="product-image-thumb active"><img src="../../dist/img/prod-1.jpg" alt="Product Image" /></div>
                                        <div className="product-image-thumb"><img src="../../dist/img/prod-2.jpg" alt="Product Image" /></div>
                                        <div className="product-image-thumb"><img src="../../dist/img/prod-3.jpg" alt="Product Image" /></div>
                                        <div className="product-image-thumb"><img src="../../dist/img/prod-4.jpg" alt="Product Image" /></div>
                                        <div className="product-image-thumb"><img src="../../dist/img/prod-5.jpg" alt="Product Image" /></div>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-6">
                                    <div className="text-center"
                                    
                                        id="titulo"
                                        //nombre= "titulo"
                                        value={titulo}
                                        >                                    
                                        
                                        </div>
                                    <p>{descripcion}</p>
                                    <hr />
                                    <h4>Available Colors</h4>
                                    <div className="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label className="btn btn-default text-center active">
                                            <input type="radio" name="color_option" id="color_option_a1" autoComplete="off" defaultChecked />
                                            Green
                                            <br />
                                            <i className="fas fa-circle fa-2x text-green" />
                                        </label>
                                        <label className="btn btn-default text-center">
                                            <input type="radio" name="color_option" id="color_option_a2" autoComplete="off" />
                                            Blue
                                            <br />
                                            <i className="fas fa-circle fa-2x text-blue" />
                                        </label>
                                        <label className="btn btn-default text-center">
                                            <input type="radio" name="color_option" id="color_option_a3" autoComplete="off" />
                                            Purple
                                            <br />
                                            <i className="fas fa-circle fa-2x text-purple" />
                                        </label>
                                        <label className="btn btn-default text-center">
                                            <input type="radio" name="color_option" id="color_option_a4" autoComplete="off" />
                                            Red
                                            <br />
                                            <i className="fas fa-circle fa-2x text-red" />
                                        </label>
                                        <label className="btn btn-default text-center">
                                            <input type="radio" name="color_option" id="color_option_a5" autoComplete="off" />
                                            Orange
                                            <br />
                                            <i className="fas fa-circle fa-2x text-orange" />
                                        </label>
                                    </div>
                                    <h4 className="mt-3">Size <small>Please select one</small></h4>
                                    <div className="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label className="btn btn-default text-center">
                                            <input type="radio" name="color_option" id="color_option_b1" autoComplete="off" />
                                            <span className="text-xl">S</span>
                                            <br />
                                            Small
                                        </label>
                                        <label className="btn btn-default text-center">
                                            <input type="radio" name="color_option" id="color_option_b2" autoComplete="off" />
                                            <span className="text-xl">M</span>
                                            <br />
                                            Medium
                                        </label>
                                        <label className="btn btn-default text-center">
                                            <input type="radio" name="color_option" id="color_option_b3" autoComplete="off" />
                                            <span className="text-xl">L</span>
                                            <br />
                                            Large
                                        </label>
                                        <label className="btn btn-default text-center">
                                            <input type="radio" name="color_option" id="color_option_b4" autoComplete="off" />
                                            <span className="text-xl">XL</span>
                                            <br />
                                            Xtra-Large
                                        </label>
                                    </div>
                                    <div className="bg-gray py-2 px-3 mt-4">
                                        <h2 className="mb-0">
                                            {"$ "+precio+"  COP"}
                                        </h2>
                                    </div>
                                    <div className="mt-4">
                                        <div className="btn btn-primary btn-lg btn-flat">
                                            <i className="fas fa-cart-plus fa-lg mr-2" />
                                            Add to Cart
                                        </div>
                                        <div className="btn btn-default btn-lg btn-flat">
                                            <i className="fas fa-heart fa-lg mr-2" />
                                            Add to Wishlist
                                        </div>
                                    </div>
                                    <div className="mt-4 product-share">
                                        <Link to={"#"} className="text-gray">
                                            <i className="fab fa-facebook-square fa-2x" />
                                        </Link>
                                        <Link to={"#"} className="text-gray">
                                            <i className="fab fa-twitter-square fa-2x" />
                                        </Link>
                                        <Link to={"#"} className="text-gray">
                                            <i className="fas fa-envelope-square fa-2x" />
                                        </Link>
                                        <Link to={"#"} className="text-gray">
                                            <i className="fas fa-rss-square fa-2x" />
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="row mt-4">
                                <nav className="w-100">
                                    <div className="nav nav-tabs" id="product-tab" role="tablist">
                                        <Link className="nav-item nav-link active" id="product-comments-tab" data-toggle="tab" to={"#"} role="tab" aria-controls="product-comments" aria-selected="true">Comments</Link>
                                        <Link className="nav-item nav-link" id="product-rating-tab" data-toggle="tab" to={"#"} role="tab" aria-controls="product-rating" aria-selected="false">Puntaje</Link>
                                    </div>
                                </nav>
                                <div className="tab-content p-3" id="nav-tabContent">
                                    <div className="tab-pane fade show active" id="product-comments" role="tabpanel" aria-labelledby="product-comments-tab"> 
                                    {
                                        comentario.map(
                                            item=> 
                                            <div><small>{item.}</small></div>
                                        )
                                    }
                                    </div>
                                    <div className="tab-pane fade" id="product-rating" role="tabpanel" aria-labelledby="product-rating-tab"> Cras ut ipsum ornare, aliquam ipsum non, posuere elit. In hac habitasse platea dictumst. Aenean elementum leo augue, id fermentum risus efficitur vel. Nulla iaculis malesuada scelerisque. Praesent vel ipsum felis. Ut molestie, purus aliquam placerat sollicitudin, mi ligula euismod neque, non bibendum nibh neque et erat. Etiam dignissim aliquam ligula, aliquet feugiat nibh rhoncus ut. Aliquam efficitur lacinia lacinia. Morbi ac molestie lectus, vitae hendrerit nisl. Nullam metus odio, malesuada in vehicula at, consectetur nec justo. Quisque suscipit odio velit, at accumsan urna vestibulum a. Proin dictum, urna ut varius consectetur, sapien justo porta lectus, at mollis nisi orci et nulla. Donec pellentesque tortor vel nisl commodo ullamcorper. Donec varius massa at semper posuere. Integer finibus orci vitae vehicula placerat. </div>
                                </div>
                            </div>
                        </div>
                        {/* /.card-body */}
                    </div>
                    {/* /.card */}
                </section>
                {/* /.content */}
            {/* /.content-wrapper */}
            {/* Control Sidebar */}
            <aside className="control-sidebar control-sidebar-dark">
                {/* Control sidebar content goes here */}
            </aside>
            {/* /.control-sidebar */}
        </div>
        {/* ./wrapper */}
        <Footer></Footer>
    </div>

    )
}

export default Anuncio;