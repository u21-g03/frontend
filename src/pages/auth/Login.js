import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import swal from "sweetalert";
import APIInvoke from "../../APIInvoke";

const Login = () =>{

    let navigate = useNavigate();

    const [user, setUser] = useState(
        {
            correo:'',
            contrasena:''
        }
    
    );

    const {correo, contrasena} = user;

    const onChange = (event) => {
        setUser(
            {
                ...user,
                [event.target.name] : event.target.value
            }
        );
    };

    const login = async () => {

        const data ={
            correo: user.correo,
            contrasena: user.contrasena 
        };

        const response = await APIInvoke.invokePOST(`/usuario/login`, data);

        if (response.message === "Incorrect Password") {

            swal({
                title: "Error",
                icon: "error",
                text: "La contraseña es incorrecta",
                buttons:{
                    confirm:{

                        text:'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

            setUser(
                {
                    contrasena:'',
                }
            );
            
        } else if (response.message === "Empty credentials") {

            swal({
                title: "Error",
                icon: "error",
                text: "El correo no está en el sistema",
                buttons:{
                    confirm: {

                        text:'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

            setUser(
                {
                    contrasena:'',
                }
            );

        } else if (response.message === "success") {

            localStorage.setItem('token', response.token);
            console.log(response.token);
            //navigate('/');

                swal({
                    title: "Bienvenido(a)",
                    icon: "success",
                    text: "Usuario y contraseña correctos",
                    buttons:{
                        confirm: {

                            text:'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-danger',
                            closeModal: true
                        }
                    }
                }).then(() => {
                    window.location = '/'                
                });;
        } else {
            
            swal({
                title: "Error",
                icon: "warning",
                text: "Error en el servidor, reintente después",
                buttons:{
                    confirm: {

                        text:'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

            setUser(
                {
                    contrasena:'',
                }
            );

        }
    };

    useEffect(() => {
        document.getElementById("correo").focus();
    }, []);

    const onSubmit = (event) => {
        event.preventDefault();
        login();
    } 



    return(
        <div className="hold-transition login-page">

            <div className="login-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <Link to="{#}" className="h1"><b>Remate de Demolición</b></Link>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Inicie Sesión</p>
                        
                        <form onSubmit = {onSubmit}>

                            <div className="input-group mb-3">
                                
                                <input type="email" 
                                className="form-control" 
                                placeholder="Correo" 
                                id="correo"
                                name="correo"
                                value={correo}
                                onChange={ onChange }
                                required
                                />
                                
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                
                                <input type="password" 
                                className="form-control" 
                                placeholder="Contraseña"
                                id="contrasena"
                                name="contrasena"
                                value={contrasena}
                                onChange= {onChange}
                                required
                                />
                                
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>

                            <div className="social-auth-links text-center mt-2 mb-3">
                                <button type="submit" className="btn btn-block btn-primary">
                                    Ingresar
                                </button>
                                <Link to= {"/register"} className="btn btn-block btn-success">
                                    Crear Cuenta Nueva
                                </Link>
                                <Link to={"/"} className= "btn btn-block btn-danger">Cancelar</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;