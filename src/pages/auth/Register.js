import React, {useState , useEffect} from "react";
import { Link } from "react-router-dom";
import APIInvoke from "../../APIInvoke";
import swal from 'sweetalert';


const Registro = () => {
    
    const [user, setUser] = useState(
        {
            nombre:'',
            cedula:'',
            telefono: '',
            correo:'',
            contrasena:'',
            confirmacion:''
        }
    );

    const {nombre, cedula, telefono, correo, contrasena, confirmacion} = user;

    const onChange = (event) => {
        console.log(user);
        setUser(
            {
            ...user,
            [event.target.name] : event.target.value
            }
        );
    };

    const onSubmit =  (event) => {
        event.preventDefault();
        nuevoUsuario();
    }

    const nuevoUsuario = async () => {
        
        if (contrasena !== confirmacion) {
            
            swal({
                title: 'Error', 
                text: 'La contraseña no coincide',
                icon: 'error',
                button: {
                    confirm: {
                        text: 'Ok',
                        value: 'true',
                        visible: 'true',
                        className: 'btn-btn-danger',
                        closeModal: true
                    }
                }
            })
            setUser(
                {
                    contrasena:'',
                    confirmacion:''
                }
            );
        } else {
            
        const data = 
        {
            nombre: user.nombre,
            cedula: user.cedula,
            contrasena: user.contrasena,
            correo: user.correo,
            telefono: user.telefono
        }
        
    const response = await APIInvoke.invokePOST(`/usuario/nuevo`, data);
        
    if (response.message === 'User already registered') {

        swal({
            title: 'Error', 
            text: 'El usuario ya existe previamente',
            icon: 'warning',
            button: {
                confirm: {
                    text: 'Ok',
                    value: 'true',
                    visible: 'true',
                    className: 'btn-btn-warning',
                    closeModal: true
                }
            }
        });

    } else if(response.message !== 'success') {
        swal({
            title: 'Error', 
            text: 'Error del servidor',
            icon: 'error',
            button: {
                confirm: {
                    text: 'Ok',
                    value: 'true',
                    visible: 'true',
                    className: 'btn-btn-success',
                    closeModal: true
                }
            }
        });
    } else {
        swal({
            title: 'Éxito', 
            text: 'Usuario creado correctamente.\nInicie sesión para continuar',
            icon: 'success',
            button: {
                confirm: {
                    text: 'Ok',
                    value: 'true',
                    visible: 'true',
                    className: 'btn-btn-success',
                    closeModal: true,

                }
            }
        }).then(function() {
            window.location = "/login";
        });
        //Limpiar textbox despues de sweetalert
        /*setUser(
            {
                name:'',
                email:'',
                password:'',
                confirm: ''
            }
        );*/
    }
}};

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, []);
    
    return(
        <div className="hold-transition register-page">
            <div className="register-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <Link to={"#"} className="h1"><b>Crear Cuenta Nueva</b></Link>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Ingrese sus Datos</p>
                        
                        <form onSubmit={onSubmit}>
                            
                            <div className="input-group mb-3">
                                <input 
                            
                                type="text" 
                                className="form-control" 
                                placeholder="Nombres y Apellidos"
                                id="nombre"
                                name="nombre"
                                value = { nombre }
                                onChange = {onChange}
                                required
                                //disabled = {true }  
                                />
                                
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input 
                            
                                type="text" 
                                className="form-control" 
                                placeholder="Cédula"
                                id="cedula"
                                name="cedula"
                                value = { cedula }
                                onChange = {onChange}
                                required
                                //disabled = {true }  
                                />
                                
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input 
                            
                                type="text" 
                                className="form-control" 
                                placeholder="Teléfono"
                                id="telefono"
                                name="telefono"
                                value = { telefono }
                                onChange = {onChange}
                                required
                                //disabled = {true }  
                                />
                                
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-phone" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input 
                                
                                type="email" 
                                className="form-control" 
                                placeholder="Correo"
                                id="correo"
                                name="correo"
                                value = { correo }
                                onChange = {onChange}
                                required
                                
                                />
                                
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input 
                                
                                type="password" 
                                className="form-control" 
                                placeholder="Contraseña"
                                id="contrasena"
                                name="contrasena"
                                value = { contrasena }
                                onChange = {onChange}
                                required
                                
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>

                            <div className="input-group mb-3">
                                <input 
                                
                                type="password" 
                                className="form-control" 
                                placeholder="Confirme la Contraseña"
                                id="confirmacion"
                                name="confirmacion"
                                value = { confirmacion }
                                onChange = {onChange}
                                required
                                
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>

                            <div className="social-auth-links text-center">
                                <button type="submit" className="btn btn-block btn-success">
                                    Crear
                                </button>
                                <Link to="/login" className="btn btn-block btn-danger">
                                    Cancelar
                                </Link>
                        </div>    
                        </form>
                        
                    </div>
                    {/* /.form-box */}
                </div>{/* /.card */}
            </div>
            {/* /.register-box */}
        </div>
        
    );
};

export default Registro;