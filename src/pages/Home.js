import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import APIInvoke from "../APIInvoke";
import Footer from "../Components/Footer";
import Navbar from "../Components/navbarDefault";

const Home = () =>{

  //  const navigate = useNavigatevigate();

    const [anuncios, setAnuncios] = useState(
        [
            
        ]
    );

    const cargarAnuncios = async () => {
        
        const response = await APIInvoke.invokeGET(`/inicio`);
        //console.log(response);
        setAnuncios(response);

    }
    
    useEffect(() => {
        cargarAnuncios();
    },[]);
    
    return(
        <div className="hold-transition sidebar-mini">
            <div className="">
            <div><Navbar></Navbar></div>
                <div >
                    <section className="content">

                        <div className="card">
                            <div className="card-header">
                                <h3 className="text-center">Anuncios publicados</h3>
                              {/*}  <div className="card-tools">
                                    <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i className="fas fa-minus" />
                                    </button>
                                    <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                        <i className="fas fa-times" />
                                    </button>
                                </div>{*/}
                            </div>
                            <div className="card-body p-0">
                                
                                <table className="table table-striped projects">
                                    
                                    <thead>
                                        <tr>

                                            <th style={{ width: '25%' }}className="text-center">
                                                Título
                                            </th>
                                            <th style={{ width: '10%' }}className="text-center">
                                                Ubicación
                                            </th>
                                            <th style={{ width: '5%' }} className="text-center">
                                                Estado
                                            </th>
                                            <th style={{ width: '5%' }} className="text-center">
                                                Cantidad
                                            </th>
                                            <th style={{ width: '10%' }}className="text-center">
                                                Categoría
                                            </th>
                                            <th style={{ width: '10%' }}className="text-center">
                                                Precio
                                            </th>
                                            <th style={{ width: '10%' }}className="text-center">
                                                <>Fecha de Publicación</>
                                            </th>
                                            
                                        </tr>
                                    </thead>

                                    <tbody>

                                    {
                                        anuncios.map(
                                            item => 
                                        <tr key={item._id}>

                                            <td className="text-center">{item.titulo}</td>
                                            <td className="text-center">{item.ubicacion}</td>
                                            <td className="text-center">{/*item.estado.toString()*/"Activo"}</td>
                                            <td className="text-center">{item.cantidad}</td>
                                            <td className="text-center">{item.categoria}</td>
                                            <td className="text-center">{item.precio}</td>
                                            <td className="text-center">{item.createdAt.split("T")[0].toString()}</td>

                                        </tr>
    
                                        )
                                    }

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>

                <Footer></Footer>

                {/* Control Sidebar */}
                <aside className="control-sidebar control-sidebar-dark">
                </aside>

            </div>
        </div>

        );
    }
    
    export default Home;