import React from 'react'
import { Link } from 'react-router-dom';

const Footer = () => {
    return (

        <footer className="main-footer">
            <div className="float-right d-none d-sm-block">
                <b>Version</b> 0.1.9
            </div>
            <strong>MISION TIC UIS 2022</strong> Proyecto con fines académicos.
        </footer>


    )
}

export default Footer;