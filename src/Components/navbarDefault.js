import React, { Component, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import APIInvoke from "../APIInvoke";

const Navbar = () => {

    const navigate = useNavigate();

    const [Buscar, setBuscar] = useState(
        {
            Buscar:''
        }
    );

    const onChange = (event) => {
                console.log(Buscar);
                setBuscar(
                    {
                    [event.target.id] : event.target.value
                    }
                );
            };
    
    const onSubmit =  (event) => {
        event.preventDefault();
        console.log("HOla");
                buscar();
    }

  /*  const logout = (e) => {
        e.preventDefault();
        localStorage.clear();
        navigate('/login');
    }*/

    const buscar = async () => {
        
        const params = 
            Buscar.Buscar
        
        console.log("params",params);
        const res = await APIInvoke.invokeGET(`/anuncio/buscar/${params}`)
        localStorage.setItem('search', res);
        console.log(res)
        

        //const response = await APIInvoke.invokeGET(`/anuncio/buscar/${Buscar}`);
        //console.log({"response":response});

    }

/*
    useEffect( () => {
    
        async function fetchData() {
            const response = await APIInvoke.invokePOST(`/`, {} );
            //const response = ""
            switch (response.message) {
                case "No":
                    console.log("no");
                    break;

                case "Si":

                console.log("Si");
                break;
            
                default:
                    console.log("otro");
                    break;
            }

            if( response.error !== undefined ){
                //navigate('/login');
                return;
            }

            //document.getElementById("nombre").innerHTML = response.data.nombre;
        }

        fetchData();

    }, []);*/

    return (

        <nav className="navbar navbar-expand navbar-primary navbar-dark">

            {/* Left navbar links */}
            <ul className="navbar-nav">

                <li className="nav-item d-none d-sm-inline-block">
                    <Link to={"/"} className="nav-link">Inicio</Link>
                </li>
                <li className="nav-item d-none d-sm-inline-block">
                    <Link to={"#"} className="nav-link"></Link>
                </li>
            </ul>

            {/* Right navbar links */}
            <ul className="navbar-nav ml-auto">

                {/* Navbar Search */}
                <li className="nav-item">
                    <Link className="nav-link" data-widget="navbar-search" data-target="#navbar-search3" href="#" role="button">
                        <i className="fas fa-search" />
                    </Link>
                    
                    <div className="navbar-search-block" id="navbar-search3">
                        
                        <form className="form-inline" onSubmit={onSubmit}>
                            <div className="input-group input-group-sm">
                                <input className="form-control form-control-navbar" 
                                
                                type="search" 
                                placeholder="Buscar" 
                                aria-label="Search" 
                                id="Buscar"
                                //value = { Buscar }
                                onChange = {onChange}
                                />

                                <div className="input-group-append">
                                    <button className="btn btn-navbar" type="submit">
                                        <i className="fas fa-search" />
                                    </button>
                                    <button className="btn btn-navbar"  type="button" data-widget="navbar-search">
                                        <i className="fas fa-times" />
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </li>

                <li className="nav-item">
                    <Link className="nav-link" data-widget="fullscreen" to={"#"} role="button">
                        <i className="fas fa-users-cog" />
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" data-widget="control-sidebar" data-slide="true" to={"/login"} role="button">
                        <button type="button" className="btn btn-block btn-light">Ingresar</button>
                    </Link>
                </li>
            </ul>
        </nav>


    )
};

export default Navbar;
